@extends('cars.layout')

@section('content')
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Modificar Auto</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-primary" href="{{ route('cars.index') }}"> Back</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> There were some problems with your input.<br><br>
  <ul>
  @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach
  </ul>
</div>
@endif

<form action="{{ route('cars.update', $car->id) }}" method="POST">
  @csrf
  @method('PUT')
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 d-none">
      <div class="form-group">
        <strong>ID: </strong>
        <input type="text" name="plate" value="{{ $car->id }}" class="form-control" disabled>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Patente: </strong>
        <input type="text" name="plate" value="{{ $car->plate }}" class="form-control" placeholder="ingresa una patente ex: PK-MN-80">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Marca: </strong>
        <input type="text" name="brand" value="{{ $car->brand }}" class="form-control" placeholder="ingresa una marca ex: Audi">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Modelo: </strong>
        <input type="text" name="model" value="{{ $car->model }}" class="form-control" placeholder="ingresa un modelo ex: R5">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Año: </strong>
        <input type="number" name="year" value="{{ $car->year }}" class="form-control" placeholder="ingresa el año de tu auto">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
</form>
@endsection