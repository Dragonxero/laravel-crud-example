@extends('cars.layout')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Laravel 7 CRUD - Autos</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-success" href="{{ route('cars.create') }}"> Create New Product</a>
      </div>
    </div>
  </div>

  @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
  @endif

  <table class="table table-bordered">
    <tr>
      <th>Patente</th>
      <th>Marca</th>
      <th>Modelo</th>
      <th>Año</th>
      <th width="280px">acciones</th>
    </tr>
    @foreach ($cars as $car)
    <tr>
      <td>{{ $car->plate }}</td>
      <td>{{ $car->brand }}</td>
      <td>{{ $car->model }}</td>
      <td>{{ $car->year }}</td>
      <td>
        <form action="{{ route('cars.destroy',$car->id) }}" method="POST">
          <a class="btn btn-info" href="{{ route('cars.show',$car->id) }}">Show</a>
          <a class="btn btn-primary" href="{{ route('cars.edit',$car->id) }}">Edit</a>
          @csrf
          @method('DELETE')
          <button type="submit" class="btn btn-danger">Delete</button>
        </form>
      </td>
    </tr>
    @endforeach
  </table>
  {!! $cars->links() !!}
@endsection