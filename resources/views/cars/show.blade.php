@extends('cars.layout')

@section('content')
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Detalles de Auto</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-primary" href="{{ route('cars.index') }}">Back</a>
    </div>
  </div>
</div> 

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 d-none">
    <div class="form-group">
      <strong>ID: </strong>
      {{ $car->id }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Patente: </strong>
      {{ $car->plate }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Marca: </strong>
      {{ $car->brand }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Modelo: </strong>
      {{ $car->model }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Año: </strong>
      {{ $car->year }}
    </div>
  </div>
</div>
@endsection