# Proyecto Funcional Basico Laravel 7

## Requisitos

-   PHP 7.4
-   Composer
-   NPM (NodeJS)

## Antes de Inicial el proyecto

-   composer install - este comando instalara todas las dependencias necesarias del proyecto que sean de PHP.
-   copiar el archivo .env.example en .env
-   crear una base de datos en mysql. (solo el create database).
-   configurar los datos de base de datos

    > DB_CONNECTION=mysql

    > DB_HOST=127.0.0.1

    > DB_PORT=3306 (o el puerto que ustedes tengas configurado en su pc).

    > DB_DATABASE= (aqui deben colocar el nombre d e la base de datos creada anteriormente).

    > DB_USERNAME= (aqui el usuario de su base de datos para la conexion).

    > DB_PASSWORD= (aqui va el password de su base de datos para la conexion).

-   luego ejecutar el siguiente comando en la raiz del proyecto.

    > php artisan migrate

-   finalmente con el siguiente comando inicial el proyecto.

    > php artisan serve

-   en algun navegador entrar en
    > http://127.0.0.1:8000/cars
    > y deberian ver el mantenedor sin ningun registro.
